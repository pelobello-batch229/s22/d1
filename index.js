

// Array Methods
// JS has built-in function and methods for arrys.

// Mutator Methods

let fruits = ["apple", "orange", "Kiwi", "Dragon Fruit"];

//  push()
// Will add element to the end part of the array
// SYNTAX: arryaName.push();

console.log("Curremt Array:");
console.log(fruits);
let fruitLength = fruits.push("Mango");
console.log(fruitLength);
console.log("Mutated array from push method");
console.log(fruits);

// Adding multiple elements to an array
fruits.push("Avocado", "Guava")
console.log	("Mutated array from push method");
console.log(fruits);

// pop()
// will remove array element to the end part of the array
let removedFruit = fruits.pop();
console.log(removedFruit)
console.log("Mutated array from pop method")
console.log(fruits)

// unshift()
// add element in the beginning of an array

fruits.unshift("Line", "Banana");
console.log("Mutated array from unshift");
console.log(fruits);

// shift()
// removes an element in the beginning part of an array
let anotherFruit = fruits.shift();
console.log(anotherFruit);
console.log("Mutated array from shift method");
console.log(fruits);

// splice()
// Simultaneouslt remove an element from a specifice index number and adds elements
// SYNTAX -> arrayName.splice(stratingIndex, deleteCount, elementsToBeAdded);

fruits.splice(1,2, "Lime", "Cherry");
console.log("Mutated array from splice method");
console.log(fruits);

// sort();
// sort the order of the array elements
fruits.sort();
console.log("Mutated array from sort method");
console.log(fruits);

// reverse();
// reverses order of the array elements
fruits.reverse();
console.log("Mutated array from reverse method");
console.log(fruits);

// Non Mutator Methods
// Unloke the mutator methods, non-mutator methods cannot modift the array,

let countries = ["US", "PH", "CAN", "SG", "TH", "PH", "FR", "DE"];
console.log(countries);

// indexOf()
// Will return the index number of the first matching element
//  if no element was found, JS will return -1
// SYNTAX --> arrayName.indexOf(searchValue, fromIndex);

let firstIndex = countries.indexOf("PH");
console.log("Result of index method: " + firstIndex);

let invalidCountry = countries.indexOf("BR");
console.log("Result of index method: " + invalidCountry);

// lastIndexOf()
// Return index number of the last matching element in an array
// SYNTAX --> arrayName.lastIndexOf(searchValue, fromIndex);

let lastIndex = countries.lastIndexOf("PH");
console.log("Result of lastIndexOf method: " + lastIndex);

// Getting the index number starting from specified index;
let lastIndexStart = countries.lastIndexOf("PH", 4);
console.log("Result of lastIndexOf method: " + lastIndexStart);

// slice()
// Portion/slices elements from an array and return a new array
// SYTEAX -> arrayName.slice(startingIndex, endingIndex)

let slicedArrayA = countries.slice(2);
console.log	("Result from slice method");
console.log(slicedArrayA);

let slicedArrayB = countries.slice(2,4);
console.log	("Result from slice method");
console.log(slicedArrayB);

// slicing of elements starting from the last elements of an array

let slicedArrayC = countries.slice(-3);
console.log	("Result from slice method");
console.log(slicedArrayC);

// toString()
// Returns an array as a string seperated by commas
// Syntax -> arrayName.toString();

let stringArray = countries.toString();
console.log	("Result from toString method");
console.log(stringArray);

// concat();
// Combines two array and return the combined result
// Syntax -> arrayA.concat(arrayB)

let taskArray1 = ["drinking html", "eat js"];
let taskArray2 = ["inhale css", "breathe sass"];
let taskArray3 = ["get git", "be node"];

let tasks = taskArray1.concat(taskArray2)
console.log("Result from concat method");
console.log(tasks);

// combining multiple arrays
console.log("Result from concat method");
let allTasks = taskArray1.concat(taskArray2, taskArray3);
console.log(allTasks);

// Combining array with elements
console.log("Result from concat method");
let combinedTasks = taskArray1.concat("smell express", "throw react");
console.log(combinedTasks);

// join()
// Return n array as a string seperated by specified separator string
// Synstax -> arrayName.join('separatorString')

let users = ["John", "Jane", "Joe", "Rober"];

console.log(users.join());
console.log(users.join(''));
console.log(users.join(' - '));

// Iteration Methods

// forEach();
//ang magigin lamn ng function parameter ay yung arrayName
// Similar to a for loop that iterates on each array elements
// Syntax -> arrayName.forEach(funtion(indiveElement){statement})
allTasks.forEach(function(task){
	console.log(task)
});

// Using forEach with conditional statement

let filteredTask = [];

allTasks.forEach(function(task){
	if(task.length >= 10){
		filteredTask.push(task);
	}
});

console.log("Result of filteredTask");
console.log(filteredTask);

// map()
// This is useful for performing tasks where mutating/chaning the elements
// syntax -> let/const resultArray = arrayName.map(function(indivElement){satement})

let numbers = [1, 2, 3, 4, 5];

let numberMap = numbers.map(function(number){
	return number * number;
});

console.log("Original Array: ");
console.log(numbers); //original is unaffected by map()
console.log("Result from map method ");
console.log(numberMap);// A new array is returned and stored in a variable

// map() vs forEach()

let numberForEach = numbers.forEach(function(number){
	return number*number;
})

console.log(numberForEach); //undefined
// forEach(),loops over all items in the array as does map(), but forEach() does not return new array
// Syntax -> let/const resultArray = arrayname.every(function(indivElement){condition})

let allValid = numbers.every((function(number){
	return (number <  3);
}))

console.log("Result from every method");
console.log(allValid);

// some()
// check if atleast one element in array meets the condition
// Syntax -> let/const resultArray = arrayName.some(function(indivElement){condition})

let someValid = numbers.some(function (number) {
	return (number < 2);
})
console.log("Result from some method");
console.log(someValid);


// filter()
// Returns new array that contains elements which meet the given condition
// Retunr an empty array if no elements were found
// Syntax -> let/const resultArray = arrayName.filter(function(indivElemt){condition})

let filterValid = numbers.filter(function(number){
	return (number < 3);
})
console.log("Result form filter method");
console.log(filterValid);

// includes ()
// Checks if the argument passed can be found in the array
// Syntax -> arrayName.includes(<argumentToFind>)

let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

let productFound1 = products.includes("Mouse");
console.log(productFound1);

let productFound2 = products.includes("Headset");
console.log(productFound2);

// Combine filter and include methods
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes("a");
})
console.log(filteredProducts);

// reduce()
// Evaluates elemets from left to rigt and returns/ reduces the array into a single value
// Syntax -> let/const reusltArray = arrayName.reduce(function(accumulator,currentValue){operation})

let iteration = 0;

let reducedArray = numbers.reduce(function(x,y){

	console.warn("Current Iteration: " + ++iteration);
	console.log("accumulator: " + x);
	console.log("current value: " + y);
	
	// The operation to redduce the array into single value
	return x + y;

})

console.log("Result of reduce method: " + reducedArray);

let list = ["Hello", "Again", "World"];

let reducedJoin = list.reduce(function(x,y){
	return x + " " +y;
})

console.log("Result of reduced method: " + reducedJoin);